import React from 'react';

class TextComponent extends React.Component {
  renderWriteable() {
    return (<input value={this.props.value} onChange={this.props.onChange.bind(this, this.props.fieldName)} />);
  }

  renderReadOnly() {
    // the properties is a string, so we have to build it up agin
    let text = Object.keys(this.props).map((k)=> {return this.props[k]}).join('');
    return (
      <span>{text}</span>
    );
  }

  render() {
    if (this.props.writable) {
      return this.renderWriteable();
    }
    return this.renderReadOnly();
  }
}

TextComponent.displayName = 'TextComponent';
export default TextComponent;
