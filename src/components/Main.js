import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ListComponent from './ListComponent.js';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
require('normalize.css/normalize.css');


class AppComponent extends React.Component {
  render() {
    return (
      <MuiThemeProvider className="index">
        <ListComponent />
      </MuiThemeProvider>
    );
  }
}

export default AppComponent;
