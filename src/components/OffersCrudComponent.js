let dot = require('dot-object');

exports.SaveOffer = function(offer) {
  dot.object(offer.properties); // convert dotted properties to object
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    let id = (offer._id || '');
    let response;

    xhr.open('POST', '/api/' + id, true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.send(JSON.stringify(offer));
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          response = (xhr && xhr.responseText ? JSON.parse(xhr.responseText) : null);
          resolve(response)
        } else {
          reject('mal')
        }
      }
    }
  });
};

exports.GetOffers = function() {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/api/', true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.send();
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          resolve(JSON.parse(xhr.responseText))
        } else {
          reject('mal')
        }
      }
    }
  });
};

exports.GetOffer = function(offerId) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/api/' + offerId, true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.send();
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          resolve(JSON.parse(xhr.responseText))
        } else {
          reject('mal')
        }
      }
    }
  });
}

exports.DeleteOffer = function(offerId) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('DELETE', '/api/' + offerId, true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.send();
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          resolve(JSON.parse(xhr.responseText))
        } else {
          reject('mal')
        }
      }
    }
  });
}
