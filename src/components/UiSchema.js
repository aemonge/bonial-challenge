import React from 'react';
import PriceComponent from './PriceComponent.js';
import ImagePointerComponent from './ImagePointerComponent.js';
import TextComponent from './TextComponent.js';
const schemaDefinitions = require('../schema/properties.json');
const injectableSchemaComponents = {
  PriceComponent: PriceComponent,
  ImagePointerComponent: ImagePointerComponent,
  TextComponent: TextComponent
};

const style = {
  dt: {
    float: 'left',
    fontWeight: 'bold'
  },
  dd: {
    float: 'right',
    margin: '0'
  },
  ddInner: {
    margin: '0'
  },
  li: {
    display: 'inline-block',
    width: '100%'
  },
  ul: {
    listStyle: 'none',
    margin: '0',
    padding: '0'
  },
  ulInner: {
    listStyle: 'none',
    margin: '0',
    padding: '0 0 0 15px'
  }
};

class UiSchema extends React.Component {
  getTag(key, value) {
    let valueTag = null;
    let specialTags = Object.keys(schemaDefinitions);
    let tagName;
    let component;

    while ( (tagName = specialTags.pop()) && valueTag === null ) {
      if ( RegExp(`${tagName}$`).test(key) ) {
        valueTag = schemaDefinitions[tagName].tag;
      }
    }

    if (valueTag) {
      component = injectableSchemaComponents[valueTag]

      if (this.props.writable) {
        return (React.createElement(component, {
          writable: true,
          fieldName: key,
          onChange: this.props.onChange,
          value: value
        }));
      } else {
        return (React.createElement(component, value));
      }
    }

    return (this.props.writable
      ?  <input value={value} onChange={this.props.onChange} />
      :  <span>{value}</span>
    );
  }

  renderRecursive(key, value, ix, parentKey) {
    let elementValue = this.getTag((parentKey || key), value);

    // If the element/propertyName is a recursive object, render recursively
    if (!elementValue && typeof value === 'object')  {
      return (
        <li style={style.li} key={ix}>
          <dt style={style.dt}>{key}: </dt>
          <dd style={style.ddInner}>
            <ul style={style.ulInner}>
              {Object.keys(value).map((childKey, childIx) => {
                childIx += (ix*10) + 1;
                return this.renderRecursive(childKey, value[childKey], childIx, key);
              })}
            </ul>
          </dd>
        </li>
      );
    }

    if (!this.props.checkHidden || !schemaDefinitions[key].hiddenInSummary) {
      return (
        <li style={style.li} key={ix}>
          <dt style={style.dt}>{key}: </dt>
          <dd style={style.dd}>{elementValue}</dd>
        </li>
      );
    }
  }

  render() {
    if (this.props === undefined || this.props === null) {
      return(<ul></ul>);
    }

    return (
      <ul style={style.ul}>
        {Object.keys(this.props.properties).map((key, ix) => (
          this.renderRecursive(key, this.props.properties[key], (ix+1))
        ))}
      </ul>
    )
  }
}
UiSchema.defaultProps = {
  checkHidden: false
};

export default UiSchema;
