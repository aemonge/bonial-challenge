/* eslint-disable no-console */
import React from 'react';
import { GridList, GridTile, Paper } from 'material-ui';
import OfferComponent from './OfferComponent.js';
import AddCircle from 'material-ui/svg-icons/content/add-circle';
import AddOfferComponent from './AddOfferComponent.js';
import { SaveOffer, GetOffers, DeleteOffer, GetOffer } from './OffersCrudComponent.js';
let dot = require('dot-object');

// const listMock = require('../mocks/list.json').offers;
const styles = {
  list: {
    minHeight: '300px'
  },
  newOffer: {
    margin      : '10px auto',
    display     : 'table',
    height      : '150px',
    width       : '150px',
    textAlign   : 'center'
  },
  newOfferButton: {
    display       : 'table-cell',
    verticalAlign : 'middle'
  },
  tile: {
    height: 'auto'
  }
};

let activeOffer = {
  id: {},
  properties: {}
}

class ListComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      name: '',
      offers: [],
      isNew: true,
      edit: false,
      activeOffer: activeOffer
    };

    GetOffers().then(this.offersGotten.bind(this), this.genericErrorHandler);
  }

  offersGotten(offers) {
    if (offers && offers.length > 0) {
      this.setState({offers: offers});
    }
  }

  genericErrorHandler() {
    console.error('Backend error');
  }

  openNewOffer() {
    this.setState({
      open: true,
      isNew: true,
      edit: false,
      activeOffer: activeOffer
    });
  }

  cancelNewEditOffer() {
    this.setState({
      open: false
    });
  }

  offerSaved(offerId) {
    let offers;
    let activeOffer = this.state.activeOffer;
    if (offerId) {
      offers = this.state.offers.slice();
      activeOffer._id = offerId;
      offers.push(activeOffer);
    } else {
      offers = this.state.offers.map((offer) => {
        if (offer._id === activeOffer._id) {
          return activeOffer;
        }
        return offer;
      })
    }

    this.setState({
      open: false,
      offers: offers,
      activeOffer: {
        id: {},
        properties: {}
      }
    });
  }

  offerDeleted(offerId) {
    let offers = this.state.offers.filter((offer) => {
      return offer._id !== offerId
    });
    this.setState({ offers: offers });
  }

  saveOffer() {
    SaveOffer(this.state.activeOffer)
      .then(this.offerSaved.bind(this), this.genericErrorHandler);
  }

  onChange(propName, e) {
    let activeOffer = this.state.activeOffer;

    activeOffer.properties[propName] = e.target.value;
    dot.object(activeOffer.properties);
    this.setState({activeOffer: activeOffer})
  }

  onDelete(id) {
    DeleteOffer(id)
      .then(this.offerDeleted.bind(this, id), this.genericErrorHandler);
  }

  editOffer(offer) {
    this.setState({
      open: true,
      isNew: false,
      edit: true,
      activeOffer: offer
    });
  }

  openOffer(offer) {
    this.setState({
      open: true,
      isNew: false,
      edit: false,
      activeOffer: offer
    });
  }

  onExpand(id) {
    GetOffer(id)
      .then(this.openOffer.bind(this), this.genericErrorHandler);
  }

  onEdit(id) {
    GetOffer(id)
      .then(this.editOffer.bind(this), this.genericErrorHandler);
  }

  render() {
    return (
      <div>
        <GridList cols={4.2} style={styles.list}>
          {this.state.offers.map((offer) =>
            <GridTile
              style={styles.tile}
              key={offer._id}
            >
              <OfferComponent
                offerId={offer._id}
                className="paper-tile"
                onDelete={this.onDelete.bind(this)}
                onExpand={this.onExpand.bind(this)}
                onEdit={this.onEdit.bind(this)}
                properties={offer.properties}
              />
            </GridTile>
          )}

          <GridTile>
            <Paper
              style={styles.newOffer}
              circle={true}
              onClick={this.openNewOffer.bind(this)}
            >
              <div style={styles.newOfferButton}>
                <AddCircle />
              </div>
            </Paper>
          </GridTile>
        </GridList>

        <AddOfferComponent
          offer={this.state.activeOffer}
          open={this.state.open}
          isNew={this.state.isNew}
          edit={this.state.edit}
          onChange={this.onChange.bind(this)}
          handleDismiss={this.cancelNewEditOffer.bind(this)}
          handleClose={this.saveOffer.bind(this)}
        />
      </div>
    );
  }
}

ListComponent.displayName = 'ListComponent';
export default ListComponent;
