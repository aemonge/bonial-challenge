import React from 'react';

class ImagePointerComponent extends React.Component {
  renderWriteable() {
    let value = this.props.value.itemName;

    return (
      <span>imageUrl <input value={value} onChange={this.props.onChange.bind(this, this.props.fieldName + '.itemName')} />
      </span>
    );
  }

  renderReadOnly() {
    let src = `/${this.props.itemName}`;
    return (
      <img src={src} />
    );
  }

  render() {
    if (this.props.writable) {
      return this.renderWriteable();
    }
    return this.renderReadOnly();
  }
}

ImagePointerComponent.displayName = 'ImagePointerComponent';
export default ImagePointerComponent;
