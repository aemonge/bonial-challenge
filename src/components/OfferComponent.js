import React from 'react';
import { Paper, Card, FloatingActionButton } from 'material-ui';
import UiSchema from './UiSchema.js';
import ActionOpenInNew from 'material-ui/svg-icons/action/open-in-new';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit';


const style = {
  Paper: {
    margin      : '10px auto',
    display     : 'block',
    width       : '300px'
  },
  Button: {
    margin : '10px',
    float  : 'right'
  },
  Card: {
    padding   : '10px'
  }
}

class OfferComponent extends React.Component {
  render() {
    return (
      <Paper style={style.Paper}>
        <FloatingActionButton style={style.Button}>
          <ActionDelete onClick={this.props.onDelete.bind(this, this.props.offerId)}/>
        </FloatingActionButton>

        <FloatingActionButton style={style.Button}>
          <EditorModeEdit onClick={this.props.onEdit.bind(this, this.props.offerId)}/>
        </FloatingActionButton>

        <FloatingActionButton style={style.Button}>
          <ActionOpenInNew onClick={this.props.onExpand.bind(this, this.props.offerId)}/>
        </FloatingActionButton>

        <Card style={style.Card}>
          <UiSchema
            checkHidden={true}
            properties={this.props.properties} />
        </Card>
      </Paper>
    );
  }
}

export default OfferComponent;
