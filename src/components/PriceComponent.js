import React from 'react';

class PriceComponent extends React.Component {
  renderWriteable() {
    let currencyCode = this.props.value.currencyCode;
    let amount = this.props.value.amount;

    return (
      <ul>
        <li>currencyCode: <input value={currencyCode} onChange={this.props.onChange.bind(this, this.props.fieldName + '.currencyCode')} /></li>
        <li>amount: <input value={amount} onChange={this.props.onChange.bind(this, this.props.fieldName + '.amount')} /></li>
      </ul>
    );
  }

  renderReadOnly() {
    return (
      <span className="price-component">
        <span>{this.props.currencyCode} </span>
        <span>{this.props.amount}</span>
      </span>
    );
  }

  render() {
    if (this.props.writable) {
      return this.renderWriteable();
    }
    return this.renderReadOnly();
  }
}

PriceComponent.displayName = 'PriceComponent';
export default PriceComponent;
