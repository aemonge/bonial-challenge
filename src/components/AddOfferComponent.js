/* eslint-disable no-console */
import React from 'react';
import { Dialog, FlatButton } from 'material-ui';
import UiSchema from './UiSchema.js';
const offerSchema = require('../schema/properties.json');

class AddOfferComponent extends React.Component {
  fillPropertiesFromSchema() {
    var activeOffer = {};
    Object.keys(offerSchema).forEach((key) => {
      activeOffer[key] = this.props.offer.properties[key] || offerSchema[key].default;
    });

    return activeOffer;
  }

  renderNew(actions) {
    return (
      <Dialog
        title="New Offer"
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}
        open={this.props.open}
      >
        <UiSchema
          writable={true}
          onChange={this.props.onChange}
          properties={this.fillPropertiesFromSchema()} />
        </Dialog>
    );
  }

  renderOffer(actions) {
    return (
      <Dialog
        title={this.props.edit ? 'Edit Offer' : 'View Offer'}
        actions={actions}
        modal={true}
        autoScrollBodyContent={true}
        open={this.props.open}
      >
        <UiSchema
          writable={this.props.edit}
          onChange={this.props.onChange}
          properties={this.fillPropertiesFromSchema()} />
        </Dialog>
    );
  }

  render() {
    let actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.props.handleDismiss}
      />
    ];

    if (this.props.edit || this.props.isNew) {
      actions.push((
        <FlatButton
          label="Submit"
          primary={true}
          onClick={this.props.handleClose}
        />
      ))
    }

    if(this.props.isNew) {
      return this.renderNew(actions);
    }

    return this.renderOffer(actions);
  }
}

AddOfferComponent.displayName = 'AddOfferComponent';

AddOfferComponent.defaultProps = {
  open: false,
  edit: false,
  isNew: true
};

export default AddOfferComponent;
