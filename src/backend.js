/*eslint no-console:0 */
'use strict';

module.exports = (function() {
  const bodyParser       = require('body-parser');
  const Mongoose         = require('mongoose');
  const collectionName   = 'offers';
  Mongoose.Promise       = require('q').Promise;

  function init(config, uiSchema) {
    let express = require('express');

    this.schema = uiSchema;
    this.config = config;
    this.app    = express();
    this.router = express.Router();
    this.offers = require('./crudBackend.js');

    initDB.call(this);
  }

  function listening(port, err) {
    if (err) {
      console.log(err);
    }
    console.log('Listening at localhost:' + port);
  }

  function initDB() {
    Mongoose.connect('mongodb://localhost/offers', {
      server: {
        auto_reconnect: true,
        socketOptions : {
          keepAlive: 1
        }
      }
    });

    this.offers.init(Mongoose.connection, collectionName, this.schema);
    Mongoose.connection.on('error', console.error.bind(console, 'Error:'));
  }

  function routes() {
    this.router.post('/', (req, res) => {
      let body = req.body;
      if (body && body.properties) {
        this.offers.create(body.properties, (id) => {
          res.send(id);
        });
      }
    });

    this.router.post('/:id', (req, res) => {
      let body = req.body;
      if (body && body.properties) {
        this.offers.update(req.params.id, body.properties, () => {
          res.send();
        });
      }
    });

    this.router.get('/', (req, res) => {
      this.offers.getAll((offers) => {
        res.send(offers);
      });
    });

    this.router.get('/:id', (req, res) => {
      this.offers.get(req.params.id, (offer) => {
        res.send(offer);
      }, () => {
        res.status(500).send({
          error: `can't find id ${req.params.id} to delete`
        });
      });
    });

    this.router.delete('/:id', (req, res) => {
      this.offers.remove(req.params.id, () => {
        res.send(true);
      }, () => {
        res.status(500).send({
          error: `can't find id ${req.params.id} to delete`
        });
      });
    });
  }

  function serve() {
    let port = this.config.port;
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());

    routes.call(this);
    this.app.use('/', this.router);
    this.app.listen(port, 'localhost', listening.bind(console, port));
  }

  return {
    init: init,
    serve: serve
  };
}());
