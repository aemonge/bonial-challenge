/*eslint no-console:0 */
'use strict';

module.exports = (function() {
  function init(mongoDB, collectionName, schema) {
    this.mongoDB = mongoDB;
    this.collectionName = collectionName;
    this.schema = schema;
    this.model = this.mongoDB.model(this.collectionName, this.schema);
  }

  function create(offer, callback) {
    this.mongoDB.collection(this.collectionName).save({
      properties: offer
    }, (error, result) => {
      if (error) return console.error(error);

      callback(result.ops[0]._id);
    });
  }

  function get(id, callback, errorCallback) {
    this.model.findOne({_id: id }, (err, model) => {
      if (err) {
        errorCallback();
        return;
      }

      callback(model);
    });
  }

  function getAll(callback) {
    this.mongoDB.collection(this.collectionName).find().toArray((err, result) => {
      if (err) return console.error(err)
        callback(result);
    });
  }

  function update(id, data, callback, errorCallback) {
    this.model.findOneAndUpdate({_id: id }, {properties: data },
      (err) => {
        if (err) {
          errorCallback();
          return;
        }
        callback(true);
      }
    );
  }

  function remove(id, callback, errorCallback) {
    this.model.findOne({_id: id }, (err, model) => {
      if (err) {
        errorCallback();
        return;
      }

      model.remove(() => {
        callback(true);
      })
    });
  }

  return {
    init: init,
    create: create,
    get: get,
    getAll: getAll,
    update: update,
    remove: remove
  };
}());
