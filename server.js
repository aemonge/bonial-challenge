/*eslint no-console:0 */
'use strict';

const uiSchema = {
  id: String,
  properties: Object
};

(function() {
  const http             = require('http');
  const httpProxy        = require('http-proxy');
  const HttpProxyRules   = require('http-proxy-rules');
  const BackEnd          = require('./src/backend.js');

  function proxy() {
    let proxyRules = new HttpProxyRules({
      rules: {
        '/api/': 'http://localhost:3001/'
      },
      default: 'http://localhost:3000'
    });

    var proxy = httpProxy.createProxy();
    http.createServer(function(req, res) {
      var target = proxyRules.match(req);
      if (target) {
        return proxy.web(req, res, {
          target: target
        });
      }
    }).listen(8000);
    console.log('Listening at localhost:' + 8000);
  }

  BackEnd.init({port: 3001}, uiSchema);
  BackEnd.serve();
  proxy();
}());
